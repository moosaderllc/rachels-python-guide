# Rachel's Python Guide

## Download all files:

On the left sidebar, click on the Downloads icon:

![Image of download icon](extra/help-downloadbutton.png)

Then, click on the **Download repository** link.

You can save all the files to your hard drive to
access the written guides and example code!
